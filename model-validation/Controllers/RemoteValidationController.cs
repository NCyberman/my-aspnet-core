﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace ModelValidation.Controllers
{
    public class RemoteValidationController : Controller
    {
        public IActionResult Index()
        {
            return View(new RegisterViewModel() { Email="Tom"});
        }

        [AcceptVerbs("Get", "Post")]
        [AllowAnonymous]
        public Task<JsonResult> IsEmailInUse(string email)
        {
            List<string> Users = new List<string>() { "Tom", "Jack", "Kate" };

            var user = Users.Where(x => x.Equals(email)).ToList();

            if (user == null)
            {
                return Task.FromResult(Json(true));
            }
            else
            {
                return Task.FromResult(Json($"Email {email} is already in use."));
            }
        }
    }
}
public class RegisterViewModel
{
    [Required]
    [EmailAddress]
    [Remote(action: "IsEmailInUse", controller: "Account")]
    public string Email { get; set; }

    // Other properties
}
