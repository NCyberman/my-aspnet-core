﻿using Microsoft.AspNetCore.Mvc;

namespace ModelValidation.Controllers
{
    public class ValidationController : Controller
    {
        [HttpGet]
        public IActionResult CustomValidation()
        {
            return View();
        }

        [HttpPost]
        public IActionResult CustomValidation(WebUserValidatable webUser)
        {
            if (ModelState.IsValid)
                return Content("Thank you!");
            else
                return View(webUser);
        }
    }
}
