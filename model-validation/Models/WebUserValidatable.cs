﻿using System.ComponentModel.DataAnnotations;

public class WebUserValidatable : IValidatableObject
{
    [Required(AllowEmptyStrings = true, ErrorMessage = "You must enter a value for the First Name field!")]
    [StringLength(25, ErrorMessage = "The First Name must be no longer than 25 characters!")]
    public string FirstName { get; set; }

    [Required(ErrorMessage = "You must enter a value for the Last Name field!")]
    [StringLength(50, MinimumLength = 3, ErrorMessage = "The Last Name must be between 3 and 50 characters long!")]
    public string LastName { get; set; }

    public DateTime Birthday { get; set; }

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    {
        if(FirstName=="Tom")
            yield return new ValidationResult("Get lost Tom?", new[] { "Birthday" });
        else if (this.Birthday.Year < 1900)
            yield return new ValidationResult("Surely you are not THAT old?", new[] { "Birthday" });
        else if (this.Birthday.Year > 2000)
            yield return new ValidationResult("Sorry, you're too young for this website!", new[] { "Birthday" });
        else if (this.Birthday.Month == 12)
            yield return new ValidationResult("Sorry, we don't accept anyone born in December!", new[] { "Birthday" });
    }
}