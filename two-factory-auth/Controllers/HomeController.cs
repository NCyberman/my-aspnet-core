﻿using Google.Authenticator;
using Microsoft.AspNetCore.Mvc;
using System.Text;
using TwoFactAuth.Models;

namespace TwoFactAuth.Controllers
{
    public class HomeController : Controller
    {
        string secretCode = string.Empty;
        public ActionResult Index()
        {
            var userEmail = "test@gmail.com";
            var twoFactorAuthenticator = new TwoFactorAuthenticator();
            secretCode = Guid.NewGuid().ToString().Replace("-", "")[0..10];
            var accountSecretKey = $"{secretCode}-{userEmail}";
            var setupCode = twoFactorAuthenticator
                .GenerateSetupCode("2FA", userEmail, Encoding.ASCII.GetBytes(accountSecretKey));

            HomeViewModel vm = new HomeViewModel
            {
                Email = userEmail,
                QrCodeUrl = setupCode.QrCodeSetupImageUrl,
                AuthCode = setupCode.ManualEntryKey,
                SecretKey = accountSecretKey
            };

            return View(vm);
        }
        [HttpPost]
        public ActionResult Index(HomeViewModel homeViewModel)
        {
            var twoFactorAuthenticator = new TwoFactorAuthenticator();
            var resultCheck = twoFactorAuthenticator.ValidateTwoFactorPIN(homeViewModel.SecretKey, homeViewModel.GoogleKey);
            if (resultCheck)
                return Content("<html><body>Ahoy. Welcome!!!</body></html>", "text/html");
            else
            {
                return View(homeViewModel);
            }
        }
    }
}
