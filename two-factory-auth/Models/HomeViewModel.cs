﻿namespace TwoFactAuth.Models
{
    public class HomeViewModel
    {
        public string Email { get; set; }
        public string QrCodeUrl { get; set; }
        public string AuthCode { get; set; }
        public string SecretKey { get; set; }
        public string GoogleKey { get; set; }
    }
}
