﻿using Microsoft.AspNetCore.Identity;
using System;

namespace CustomIdentityApp.Models
{
    public class User : IdentityUser
    {
        //public int Year { get; set; }
        public DateTime DateCreated { get; internal set; }
    }
}