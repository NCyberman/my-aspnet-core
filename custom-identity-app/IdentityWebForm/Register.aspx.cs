﻿using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using IdentityWebForm.Models;

namespace IdentityWebForm
{
    public partial class Register : System.Web.UI.Page
    {
        ApplicationDbContext applicationDbContext { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            applicationDbContext = new ApplicationDbContext();
        }
        protected void CreateUser_Click(object sender, EventArgs e)
        {

            // Default UserStore constructor uses the default connection string named: DefaultConnection
            var userStore = new UserStore<ApplicationUser>(applicationDbContext);
            var manager = new ApplicationUserManager(userStore);

            var user = new ApplicationUser()
            {
                UserName = UserName.Text,
                LastName = LastName.Text,
                FirstName = FirstName.Text,
                Email = Email.Text,
            };
            IdentityResult result = new IdentityResult();
            try
            {
                ApplicationUser findUser = applicationDbContext.Users.Where(x => x.Email == Email.Text).FirstOrDefault();
                if (findUser != null)
                    result = manager.Update(user);
                else
                    result = manager.Create(user, Password.Text);
            }
            catch
            {

            }

            if (result.Succeeded)
            {
                StatusMessage.Text = string.Format("User {0} was created successfully!", user.UserName);
            }
            else
            {
                StatusMessage.Text = result.Errors.FirstOrDefault();
            }
        }

        protected void ChangePassword_Click(object sender, EventArgs e)
        {
            try
            {
                ApplicationUser findUser = applicationDbContext.Users.Where(x => x.Email == Email.Text).FirstOrDefault();
                var userStore = new UserStore<ApplicationUser>(applicationDbContext);
                var manager = new ApplicationUserManager(userStore);
                var result = manager.AddPassword(findUser.Id, Password.Text);
                if (result.Succeeded)
                {
                    StatusMessage.Text = "Password changed";
                }
            }
            catch
            {
                StatusMessage.Text = "Exception error";
            }
        }
    }
}