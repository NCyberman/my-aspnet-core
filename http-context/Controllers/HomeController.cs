﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;

namespace http_accessor.Controllers
{
    public class HomeController : Controller
    {
        public HomeController()
        {

        }
        public IActionResult Index()
        {
            string remoteIp = HttpContext.Connection.RemoteIpAddress.MapToIPv4()?.ToString();
            string remotePort = HttpContext.Connection.RemotePort.ToString();
            string localIp = HttpContext.Connection.LocalIpAddress.MapToIPv4()?.ToString();
            string localPort = HttpContext.Connection.LocalPort.ToString();

            string remoteIp2 = HttpContext.Request.HttpContext.Features.Get<IHttpConnectionFeature>().RemoteIpAddress.ToString();
            string remotePort2 = HttpContext.Request.HttpContext.Features.Get<IHttpConnectionFeature>().RemotePort.ToString();
            string localIp2 = HttpContext.Request.HttpContext.Features.Get<IHttpConnectionFeature>().LocalIpAddress.ToString();
            string localPort2 = HttpContext.Request.HttpContext.Features.Get<IHttpConnectionFeature>().LocalPort.ToString();

            List<InfoViewModel> list = new List<InfoViewModel>()
            {
                new InfoViewModel {Name="remoteIp",Description=remoteIp},
                new InfoViewModel {Name="remotePort",Description=remotePort},
                new InfoViewModel {Name="localIp",Description=localIp},
                new InfoViewModel {Name="localPort",Description=localPort},

                new InfoViewModel {Name="remoteIp2",Description=remoteIp2},
                new InfoViewModel {Name="remotePort2",Description=remotePort2},
                new InfoViewModel {Name="localIp2",Description=localIp2},
                new InfoViewModel {Name="localPort2",Description=localPort2},
            };

            return View(list);
        }

    }
    public class InfoViewModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
