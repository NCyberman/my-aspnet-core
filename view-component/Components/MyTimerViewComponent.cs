﻿using Microsoft.AspNetCore.Mvc;

namespace view_component.Components
{
    [ViewComponent]
    public class MyTimer
    {
        public string Invoke(bool includeSeconds, bool format24)
        {
            DateTime now = DateTime.Now;

            string time = format24 ? now.ToString("HH:mm") : now.ToString("hh:mm");

            if (includeSeconds) // если надо добавить секунды
            {
                time = $"{time}:{now.Second}";
            }

            return $"Текущее время: {time}";
        }
    }
}
