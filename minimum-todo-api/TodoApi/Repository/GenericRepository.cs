﻿using Microsoft.EntityFrameworkCore;

namespace TodoApi.Repository
{
    public class GenericRepository<T> : IGenericRepository<T> where T : class
    {
        private TodoDbContext _context;
        private DbSet<T> table = null;

        public GenericRepository(TodoDbContext _context)
        {
            this._context = _context;
            table = _context.Set<T>();
        }
        public async ValueTask<IEnumerable<T>> GetAll()
        {
            return await table.ToListAsync();
        }
        public async ValueTask<T> GetById(object id)
        {
            return await table.FindAsync(id);
        }
        public async ValueTask Insert(T obj)
        {
            await table.AddAsync(obj);
        }
        public async ValueTask Update(T obj)
        {
            _context.ChangeTracker.Clear();
            table.Attach(obj);
            _context.Entry(obj).State = EntityState.Modified;
            await ValueTask.CompletedTask;
        }
        public async ValueTask Delete(object id)
        {

            T? existing = await table.FindAsync(id);
            if (existing != null)
                table.Remove(existing);
        }
        public async ValueTask Save()
        {
            await _context.SaveChangesAsync();
        }
    }
}
