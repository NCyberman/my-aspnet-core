﻿namespace TodoApi.Repository
{
    public interface IGenericRepository<T> where T : class
    {
        ValueTask<IEnumerable<T>> GetAll();
        ValueTask<T> GetById(object id);
        ValueTask Insert(T obj);
        ValueTask Update(T obj);
        ValueTask Delete(object id);
        ValueTask Save();
    }
}
